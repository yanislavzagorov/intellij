public class Main {

    public static void main(String[] args)
    {
        int[] array = {40, 2, 5, 11, 500, 22, 1, 66};

        System.out.println("Maksimal verdi er: " + maks(array));
        System.out.println("Minimal verdi er: " + min(array));
    }

    public static int maks(int[] a)
    {
        int s = 0; //største verdi

        for (int m = 1; m < a.length; m++)
        {
            if (a[m] > a[s])
            {
                s = m;
            }
        }
        return a[s];
    }

    public static int min(int[] a)
    {
        int m = 0; //minste verdi

        for (int i = 1; i < a.length; i++)
        {
            if(a[m] > a[i]){
                m = i;
            }
        }
        return a[m];
    }

}
